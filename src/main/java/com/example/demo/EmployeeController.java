package com.example.demo;

import java.util.*;

import com.example.demo.domain.Employee;
import com.example.demo.service.EmployeeService;
import org.springframework.beans.factory.annotation.*;
import org.springframework.http.*;

import org.springframework.web.bind.annotation.*;

@RestController
public class EmployeeController {

    @Autowired
    private EmployeeService service;


    // RESTful API methods for Retrieval operations

    @GetMapping("/employees")
    public List<Employee> list() {
        System.out.println("TEST!!!!!!!!!!");
        return service.listAll();
    }

    @GetMapping("/employees/{id}")
    public ResponseEntity<Employee> get(@PathVariable Integer id) {
        try {
            Employee employee = service.get(id);
            return new ResponseEntity<Employee>(employee, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
        }
    }

    // RESTful API method for Create operation

    @PostMapping(path = "/saveemployees", consumes = "application/json", produces = "application/json")
    public void add(@RequestBody Employee employee) {
        System.out.println("TEST!!!!!!!!!!");
        service.save(employee);
    }

    // RESTful API method for Update operation

    @PutMapping(path = "/employees/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> update(@RequestBody Employee employee, @PathVariable Integer id) {
        try {
            Employee existEmployee = service.get(id);
            existEmployee.setName(employee.getName());
            existEmployee.setPhone(employee.getPhone());
            service.save(existEmployee);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // RESTful API method for Delete operation

    @DeleteMapping("/employees/{id}")
    public void delete(@PathVariable Integer id) {
        service.delete(id);
    }

}